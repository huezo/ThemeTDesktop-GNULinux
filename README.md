[![AUR](https://img.shields.io/aur/license/yaourt.svg)]()

# Theme Telegram Desktop ( Theme GNU/Linux ) 

Tema Telegram Desktop (Tema de GNU/Linux )

LICENSE GPL v3

LICENCIA GPL v3


# Repository ( Respositorio ) :

##  [https://gitlab.com/huezo/ThemeTDesktop-GNULinux/tree/master](https://gitlab.com/huezo/ThemeTDesktop-GNULinux/tree/master)



## Web Site ( Sitio Web ) : 


[https://huezohuezo1990.wordpress.com/](https://huezohuezo1990.wordpress.com/)




[GNU_Linux]: https://gitlab.com/huezo/ThemeTDesktop-GNULinux/raw/55fbcf345f83766140d70237b143dde31e5aa807/demo.png




## GNU Linux Theme
![GNU Linux Theme][GNU_Linux]


## github:

[https://github.com/huezo](https://github.com/huezo)

## github theme GNU/Linux

## [https://github.com/huezo/ThemeTDesktop-GNULinux](https://github.com/huezo/ThemeTDesktop-GNULinux)


# Telegram 
Channel / Canal

[https://t.me/ThemesTelegramTemas](https://t.me/ThemesTelegramTemas)

user / usuario 

[https://t.me/huezohuezo1990](https://t.me/huezohuezo1990)



